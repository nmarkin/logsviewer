function controller() {
  var logsContainer = $("#logsContainer")
    , reMatches = {}
  ;

  function findStr(str) {
    var strFound; 
   
    if (window.find) {
      strFound = self.find(str);
      if (!strFound) {
        strFound = self.find(str, 0, 1);
        while (self.find(str, 0, 1)) continue;
      }
    } else {
      alert("Find a better browser.");
    }
  }

  function addHighlighter(highlighter) {
    var span = document.createElement("span");
    span.appendChild(document.createTextNode(highlighter.re.toString()));
    span.classList.add(highlighter.class); 
    span.onclick = function(e) {
      var i = reMatches[e.target.childNodes[0].wholeText].index += e.ctrlKey ? -1 : 1;
      if (reMatches[e.target.childNodes[0].wholeText].values[i]) {
        findStr(reMatches[e.target.childNodes[0].wholeText].values[i]);
      } else if (i == reMatches[e.target.childNodes[0].wholeText].values[i].length) {
        reMatches[e.target.childNodes[0].wholeText].index--;
      } else if (i < 0) {
        reMatches[e.target.childNodes[0].wholeText].index = 0;
      }
    }
    this.map.appendChild(span);
  }

  function applyHighlighter(re, cssClass) {
    addHighlighter({re: re, class: cssClass});
    $("#logsContainer li div").each(function(index, div) {
      var match = div.innerHTML.match(re);

      if (match) {
        reMatches[re] = reMatches[re] ? reMatches[re] : {values: [], index: 0};
        reMatches[re].values.push(match[0]);
        div.classList.add(cssClass);
      }
    });
  }

  $("#add").click(function() {
    var minimizedHeight = "50px"
      , highlighters = window.highlighters && window.highlighters.length ? window.highlighters : [
          {re: /[\dA-Z]{17}/, class: "ohterVin"},
          {re: /[\da-z]+(-[\da-z]+){4}/, class: "otherSession"},
          // {re: /3C3CFFAR6CT354872/, class: "vin"},
          // {re: /53e068b0-c1d8-11e4-a3e3-05d3c88bd541/, class: "session"},
          {re: /(SELECT|INSERT|UPDATE|DELETE|ALTER)/, class: "sql"},
          {re: /response: dataSize:/, class: "s1"},
          {re: /_podRespQ/, class: "hideLine"},
          {re: /_podReqQ/, class: "hideLine"},
          {re: /request:/, class: "s4"},
          {re: /Handling.*request/, class: "s5"},
          {re: /Handled.*response/, class: "s6"},
          {re: /vitals/, class: "hideLine"},
          {re: /ping ecus/i, class: "hideLine"}
        ] 
    ;

    var map = document.createElement("div");
    map.setAttribute("id", "map");
    addHighlighter = addHighlighter.bind({map: map});
    map.ondblclick = function() {
      var $map = $("#map")
        , direction = $map.position().left > $(window).width() - 100 ? -1 : 1
        , right = $map.position().left + ($map.width() + parseInt($map.css("padding-left")) + parseInt($map.css("padding-right")) - 10) * direction
      ;

      $("#map").animate({left: right}, {duration: 500, queue: false});
    }
    
    highlighters.forEach(addHighlighter);
    var msg = (function() {
      var div = document.createElement("div");
      div.classList.add("msg");
      return div;
    })();

    var newHighlighterForm = (function() {
      function actionMsg(body, hide) {
        function _hide() {
          this.innerHTML = "";
          this.style.display = "none";
        }
        _hide = _hide.bind(msg);

        if (hide) {
          return _hide();
        }

        msg.innerHTML = body;
        msg.style.display = "block";
        
        setTimeout(_hide, 5000);
      }
      function reCheck() { try { var re = new RegExp(this.value); } catch(e) { return actionMsg("Invalid regular expression"); } actionMsg(null, true); return re; }

      var div = document.createElement("div");
      var cssClassRe = /^\.([a-zA-Z\d\-_]+)\s{([^\s]+:\s?.+[;\s]*)+}$/;

      div.appendChild((function() {
        var input = document.createElement("input");
        input.setAttribute("id", "newHighlighterRe");
        input.classList.add("input");;
        input.setAttribute("type", "text");
        input.onchange = reCheck;

        return input;
      })());

      var sample = (function() {
        var div = document.createElement("div");
        div.classList.add("highlighterSample");
        div.classList.add("input");

        return div;
      })();

      var newHighlighterClasses = document.createElement("style");
      newHighlighterClasses.type = "text/css";
      document.head.appendChild(newHighlighterClasses);

      div.appendChild((function() {
        var input = document.createElement("input"), firstSample = null, listId = "defaultHighlighterClasses";
        input.setAttribute("id", "newHighlighterClass");
        input.setAttribute("list", listId);
        input.onchange = function(e) {
          var matches = e.target.value.match(cssClassRe), className = e.target.value;
          if (matches) {
            newHighlighterClasses.appendChild(document.createTextNode(e.target.value));
            className = matches[1];
            var option = document.createElement("option");
            option.value = className;
            dataList.appendChild(option);
          }
          sample.className = className + " input";
        }
        var dataList = document.createElement("datalist");
        dataList.setAttribute("id", listId);
        [].slice.call(document.styleSheets, 0).forEach(function(styleSheet) {
          if (/highlighterClasses.css$/.test(styleSheet.href)) {
            [].slice.call(styleSheet.cssRules, 0).forEach(function(rule) {
              if (/^\.[a-zA-Z][a-zA-Z\-_0-9]*$/.test(rule.selectorText)) {
                var option = document.createElement("option");
                option.value = rule.selectorText.replace(".", "");
                dataList.appendChild(option);
                
                if (!firstSample) {
                  sample.classList.add(option.value);
                  sample.appendChild(document.createTextNode(option.value));
                  firstSample = option.value;
                }
              }
            });            
          }
        });
        var wrapper = document.createElement("div");
        wrapper.classList.add("input");
        wrapper.appendChild(input);
        wrapper.appendChild(dataList);

        return wrapper;
      })());

      div.appendChild(sample);

      div.appendChild((function() { 
        var button = document.createElement("button");
        button.setAttribute("id", "appendNewHighlighter");
        button.setAttribute("type", "submit");
        button.appendChild(document.createTextNode("Append"));
        button.onclick = function() {
          // implement class reassign feature
          var className = $("#newHighlighterClass").val(), matches = className.match(cssClassRe), re = $("#newHighlighterRe").val();
          if (matches) {
            className = matches[1];
          }
          if (!((re = reCheck.apply({value: re})) instanceof RegExp)) {
            return;
          }
          if (!/^[a-zA-Z\-_\d]+$/.test(className)) {
            return actionMsg("Invalid class name");
          }
          applyHighlighter(re, className);
        };

        return button;
      })());
      
      return div;
    })();
    map.appendChild(newHighlighterForm);
    map.appendChild(msg);
    logsContainer.append(map);

    // smooth scroll
    // var mapTopStyle = $("#map").css("top");
    // var mapYloc = parseInt(mapTopStyle.substring(0, mapTopStyle.indexOf("px")));
    // $(window).scroll(function () {
    //   var offset = mapYloc + $(document).scrollTop() + "px";
    //   $("#map").animate({top: offset}, {duration: 500, queue: false});
    // });

    document.querySelector("#logs").value.split(/[\r\n]/).forEach(function(line) {
      var div = document.createElement("div");
      div.appendChild(document.createTextNode(line));
      highlighters.forEach(function(highlighter) {
        var match = div.innerHTML.match(highlighter.re);

        if (match) {
          reMatches[highlighter.re] = reMatches[highlighter.re] ? reMatches[highlighter.re] : {values: [], index: 0};
          reMatches[highlighter.re].values.push(match[0]);
          if (!div.classList.contains(highlighter.class)) {
            div.classList.add(highlighter.class);
          }
        }
      });
      var li = document.createElement("li")
      li.appendChild(div);
      logsContainer.append(li);
      div.onclick = (function(_origHeight) {
        return function() {
          this.style.height = parseInt(this.style.height) != _origHeight ? "$heightpx".replace("$height", _origHeight) : minimizedHeight;
        };
      })(div.offsetHeight); 
      div.style.height = minimizedHeight; 
    });    
	});

	$("#clear").click(function() {
    logsContainer.html("");
  });

  $("#highlight").click(function() {
    var values = {
        vin: {curr: {value: $("#currVin").val(), cssc: "vin"}, prev: {value: $("#prevVin").val(), cssc: "otherVin"}, next: {value: $("#nextVin").val(), cssc: "otherVin"}}
      , vehSessonId: {curr: {value: $("#vehSessionId").val(), cssc: "session"}, prev: {value: $("#prevVehSessionId").val(), cssc: "otherSession"}, next: {value: $("#nextVehSessionId").val(), cssc: "otherSession"}}
      , cliSessonId: {curr: {value: $("#cliSessionId").val(), cssc: "clisession"}, prev: {value: $("#prevCliSessionId").val(), cssc: "otherSession"}, next: {value: $("#prevCliSessionId").val(), cssc: "otherSession"}}
      }
    ;

    Object.keys(values).forEach(function(key) {
      var re;

      if (values[key].curr.value && values[key].curr.cssc) {
        try { re = new RegExp(values[key].curr.value); } catch(e) { return alert("Invalid regular expression for current $field field".replace("$field", key)); }
        applyHighlighter(re, values[key].curr.cssc);
      }

      if (values[key].prev.value && values[key].prev.cssc) {
        try { re = new RegExp(values[key].prev.value); } catch(e) { return alert("Invalid regular expression for previous $field field".replace("$field", key)); }
        applyHighlighter(re, values[key].prev.cssc);
      }

      if (values[key].next.value && values[key].next.cssc) {
        try { re = new RegExp(values[key].next.value); } catch(e) { return alert("Invalid regular expression for next $field field".replace("$field", key)); }
        applyHighlighter(re, values[key].next.cssc);
      }
    });
  });
}