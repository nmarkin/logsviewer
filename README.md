# Log viewer.

## Kibana exporter (kibana.js):

Usage:

1. Change your paging settings, set "Per Page" to 100  and "Page Limit" to 100, enter your search query
2. Select all from this file -> copy -> paste into the console of kibana page
3. Copy a value from appeared textarea, press Esc

Bookmarklet:
```
<a href='javascript: (function () { var logsContainer = document.createElement("textarea"); logsContainer.setAttribute("style", "position: fixed; width: 500px; height: 300px; display: none; top: 150px; left: 50%; margin-left: -250px; z-index: 9999;"); logsContainer.onkeydown = function(e) { if (e.keyCode == 27) { logsContainer ? document.body.removeChild(logsContainer) : null; logsContainer = null; } }; document.body.appendChild(logsContainer); logsContainer.appendChild(document.createTextNode(angular.element(document.querySelectorAll(".kibana-container div[type=\"panel.type\"] div.ng-scope[ng-controller=\"table\"]")[0]).scope().data.reduce(function(output, line) { return output.push([line._source["@timestamp"], line._source.severity_label, line._source.message].join(" ")) && output; }, []).join("\n"))); logsContainer.style.display = "block"; })()'>Export logs</a>
```

## Viewer:

Open the controller.js, set all regular expressions for highlighting, open index.html in firefox (chrome doesn't allow to list css classes for some reason), paste log records, wait ready.